--Javier Fumanal Idocin 684229 Eduardo Criado Mascaray 662844
module BinaryTree where
data Tree a= Vacio | Leaf a | Branch a (Tree a) (Tree a)

--Saca en preorden
instance (Show a) => Show (Tree a) where
	show Vacio = " "
	show (Leaf x) = show x
	show (Branch p l r) = (show p) ++ (show l) ++ (show r)

leaf :: a->Tree a
leaf a= Leaf a

tree :: a->Tree a->Tree a->Tree a
tree a hi hd=Branch a hi hd

empty :: Tree a
empty = Vacio

size :: Tree a->Integer
size (Vacio) = 0
size (Leaf a) = 1
size (Branch a b c) = 1 + size (b) + size(c)

preorder :: Tree t->[t]
preorder (Vacio) = []
preorder (Leaf a) = [a]
preorder (Branch a b c) = [a] ++ (preorder b) ++ (preorder c)

inorder :: Tree t->[t]
inorder (Vacio) = []
inorder (Leaf a) = [a]
inorder (Branch a b c) = (inorder b) ++ [a] ++ (inorder c)

postorder :: Tree t->[t]
postorder (Vacio) = []
postorder (Leaf a) = [a]
postorder (Branch a b c) =  (postorder b) ++ (postorder c) ++ [a]

add :: (Ord t)=>Tree t->t->Tree t
add (Vacio) e= Leaf e
add (Branch x hi hd) e = if e<=x then (Branch x (add hi e) hd) else (Branch x hi (add hd e))
add (Leaf x) e= if e<=x then (Branch x (Leaf e) (Vacio)) else (Branch x (Vacio) (Leaf e))

between :: (Ord t)=>Tree t->t->t->[t]
between (Vacio) _ _= []
between (Branch x hi hd) min max
	| x<min = between hd min max
	| x>max = between hi min max
	| otherwise = (between hi min max) ++ [x] ++ (between hd min max)
between (Leaf x) min max
	| x<min = []
	| x>max = []
	| otherwise = [x]





