module BinaryTree where
data Tree a= Leaf a | Branch a (Tree a) (Tree a)

instance (Show a) => Show (Tree a) where
    show (Leaf x) = "mensajito"
    show (Branch p l r) = "masajito"

Leaf :: (Show a)=>a->Tree a
Leaf a= Leaf a

tree :: a->Tree->Tree->Tree
tree a hi hd=Branch a (hi) (hi)

empty :: Tree
empty = Tree

size_t :: Tree->Integer
size_t (Leaf a) = 1
size_t (Branch a b c) = 1 + size_t (b) + size_t(c)

